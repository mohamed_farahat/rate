package com.example.soldiers.rate;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import com.example.soldiers.rate.model.User;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {



    GridView gridView;

    private int[] Image = {R.drawable.image1,R.drawable.image2,R.drawable.image3};
    private ArrayList<User> beans;
    private GridBaseAdapter gridBaseAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        gridView= (GridView)findViewById(R.id.gridview);
        beans= new ArrayList<User>();


        for (int i= 0; i< Image.length; i++) {

            User user = new User(Image[i]);
            beans.add(user);

        }

   gridBaseAdapter = new GridBaseAdapter(MainActivity.this, beans);

        gridView.setAdapter(gridBaseAdapter);
    }
}
