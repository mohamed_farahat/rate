package com.example.soldiers.rate.model;

/**
 * Created by Rp on 3/22/2016.
 */
public class User {
    private String userName;
    private String userEmail;
    private String userPhone;
    private int image;

    public User(int image) {
        this.image = image;

    }

    public User(String userName, String userEmail, String userPhone) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPhone = userPhone;
    }

    public User(String userName, String userEmail, String userPhone, int image) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPhone = userPhone;
        this.image = image;
    }

    public User() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
