package com.example.soldiers.rate.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.soldiers.rate.MainActivity;
import com.example.soldiers.rate.R;
import com.example.soldiers.rate.model.User;
import com.example.soldiers.rate.utils.Constant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupActivty extends AppCompatActivity {

    private static final String TAG = SignupActivty.class.getSimpleName();
    private TextView mTextViewLogin;
    private EditText mEditTextEmailCreate, mEditTextUsernameCreate, mEditTextPasswordCreate, mEditTextPhoneCreate;
    private String mUserName, mUserEmail, mPassword, mPhone;
    private ProgressDialog mAuthProgressDialog;

    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private DatabaseReference userRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_activty);
        initializeScreen();

        mTextViewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignupActivty.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    public void initializeScreen() {
        mTextViewLogin = (TextView) findViewById(R.id.login);
        mEditTextEmailCreate = (EditText) findViewById(R.id.email);
        mEditTextUsernameCreate = (EditText) findViewById(R.id.user);
        mEditTextPasswordCreate = (EditText) findViewById(R.id.pass);
        mEditTextPhoneCreate = (EditText) findViewById(R.id.mob);

//Firebase references
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        userRef = mDatabase.getReferenceFromUrl(Constant.FIREBASE_URL_USERS);

 /* Setup the progress dialog that is displayed later when authenticating with Firebase */
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle(getResources().getString(R.string.progress_dialog_loading));
        mAuthProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_creating_user_with_firebase));
        mAuthProgressDialog.setCancelable(false);
    }

    private boolean isEmailValid(String email) {
        boolean isGoodEmail =
                (email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches());
        if (!isGoodEmail) {
            mEditTextEmailCreate.setError(String.format(getString(R.string.error_invalid_email_not_valid),
                    email));
            return false;
        }
        return isGoodEmail;
    }

    private boolean isUserNameValid(String userName) {
        if (userName.equals("")) {
            mEditTextUsernameCreate.setError(getResources().getString(R.string.error_cannot_be_empty));
            return false;
        }
        return true;
    }

    private boolean isPasswordValid(String password) {
        if (password.length() < 6) {
            mEditTextPasswordCreate.setError(getResources().getString(R.string.error_invalid_password_not_valid));
            return false;
        }
        return true;
    }

    private boolean isPhoneValid(String phone) {
        if (phone.length() < 11 || phone == null) {
            mEditTextPhoneCreate.setError(getResources().getString(R.string.error_invalid_phone_not_valid));
            return false;
        }
        return true;
    }

    public void onCreateAccountPressed(View view) {
        mUserName = mEditTextUsernameCreate.getText().toString();
        mUserEmail = mEditTextEmailCreate.getText().toString().toLowerCase();
        mPassword = mEditTextPasswordCreate.getText().toString();
        mPhone = mEditTextPhoneCreate.getText().toString();

        /**
         * Check that email and user name are okay
         */

        boolean validEmail = isEmailValid(mUserEmail);
        boolean validUserName = isUserNameValid(mUserName);
        boolean validPassword = isPasswordValid(mPassword);
        boolean validPhone = isPhoneValid(mPhone);

        if (!validEmail || !validUserName || !validPassword || !validPhone) return;

        mAuthProgressDialog.show();

        /**
         * If everything was valid show the progress dialog to indicate that
         * account creation has started
         */
        mAuth.createUserWithEmailAndPassword(mUserEmail, mPassword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                        mAuthProgressDialog.dismiss();

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {

                            try {
                                throw task.getException();
                            } catch (FirebaseAuthWeakPasswordException e) {
                                mEditTextPasswordCreate.setError(getString(R.string.error_invalid_password_not_valid));
                                mEditTextPasswordCreate.requestFocus();
                            } catch (FirebaseAuthInvalidCredentialsException e) {
                                mEditTextEmailCreate.setError(String.format(getString(R.string.error_invalid_email_not_valid),
                                        mUserEmail));
                                mEditTextEmailCreate.requestFocus();
                            } catch (FirebaseAuthUserCollisionException e) {
                                mEditTextEmailCreate.setError(getString(R.string.error_email_taken));
                                mEditTextEmailCreate.requestFocus();
                            } catch (FirebaseNetworkException e) {
                                showErrorToast(getString(R.string.network_error));
                            } catch (Exception e) {
                                Log.e(TAG, e.getMessage());
                            }
                            //showErrorToast(getString(R.string.log_error_occurred));
                        } else {

                            String uid = task.getResult().getUser().getUid();
                            Log.d("hhhh", uid);
                            createUserInFirebase(uid);
                        }

                        // ...
                    }
                });
    }

    private void createUserInFirebase(String id) {
        User user = new User(mUserName, mUserEmail, mPhone);
        userRef.child(id).setValue(user);
        Intent intent = new Intent(SignupActivty.this, MainActivity.class);
        startActivity(intent);

    }

    /**
     * Show error toast to users
     */

    private void showErrorToast(String message) {
        Toast.makeText(SignupActivty.this, message, Toast.LENGTH_LONG).show();

    }

}
