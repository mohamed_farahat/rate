package com.example.soldiers.rate.utils;

import com.example.soldiers.rate.BuildConfig;

/**
 * Created by Soldiers on 10/3/2017.
 */

public class Constant {

    public static final String FIREBASE_LOCATION_USERS = "users";

    /**
     * Constants for Firebase Database URL
     */
    public static final String FIREBASE_URL = BuildConfig.UNIQUE_FIREBASE_DATABASE_ROOT_URL;
    public static final String FIREBASE_URL_USERS = FIREBASE_URL + "/" + FIREBASE_LOCATION_USERS;
}
